var mdbClient = require('mongodb').MongoClient;
var http = require('http');

exports.index = function(req, res) {
    res.render("index", {
        // Template data
        title: "Fashion Store homepage"
    });
};

exports.category = function(req, res) {
    var id = req.params.category;
    mdbClient.connect("mongodb://localhost:27017/store", function(err, db) {
        var collection = db.collection('categories');

        collection.findOne({
            id: id
        }, function(err, category) {
            if (category) {
                res.render('category', category);
            } else {
                res.send('category cannot be found');
            }
        });
    });
};

exports.subcategory = function(req, res) {
    var subcategoryName = req.params.subcategory;
    mdbClient.connect("mongodb://localhost:27017/store", function(err, db) {
        var collection = db.collection('products');
        collection.find({
            primary_category_id: subcategoryName
        }).toArray(function(err, products) {
            if (products.length > 0) {
                res.render('products', {
                    products: products
                });
            } else {
                res.send('No products');
            }
        });
    });
};

exports.product = function(req, res) {
    getCurrencies(updateCurrencies);
    var id = req.params.product;
    mdbClient.connect("mongodb://localhost:27017/store", function(err, db) {
        var currencies = db.collection('currencies');
        var collection = db.collection('products');
        var listOfCurrencies = [];
        var listOfCurrenciesValues = [];
        currencies.findOne({}, function(err, currencies) {
            collection.findOne({
                id: id
            }, function(err, product) {
                if (product) {
                    for (var currency in currencies.quotes) {
                        if ((currency.slice(3, currency.length) === 'USD')) {
                            continue;
                        }
                        listOfCurrencies.push(currency.slice(3, currency.length));
                        listOfCurrenciesValues.push((currencies.quotes[currency] * product.price).toFixed(2));
                    }
                    res.render('product', {
                        product: product,
                        listOfCurrencies: listOfCurrencies,
                        listOfCurrenciesValues: listOfCurrenciesValues
                    });
                } else {
                    res.send('Cannot find product');
                }
            });
        });

    });
};

function getCurrencies(callback) {
    http.get({
        host: 'www.apilayer.net',
        path: '/api/live?access_key=e4e01d72ba6110b9a7f4f2169599f314'
    }, function(response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
            parsed = JSON.parse(body);
            callback(parsed);
        });
    });
}

function updateCurrencies(value) {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    var currentDate = year + "/" + month + "/" + day;

    mdbClient.connect("mongodb://localhost:27017/store", function(err, db) {
        var currencies = db.collection('currencies');
        currencies.findOne({}, function(err, obj) {
            if (obj.lastUpdate !== currentDate) {
                currencies.update({}, {
                    $set: {
                        source: value.source,
                        quotes: value.quotes,
                        lastUpdate: currentDate
                    }
                });

                console.log('Date is updated.');
            }
        });

    });
}
